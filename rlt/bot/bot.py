import logging
import json
import os

from aiogram import Bot, Dispatcher, types
from aiogram.filters import Command, CommandStart

from services.data_aggregator import aggregate_data

bot = Bot(token=os.getenv("BOT_TOKEN"))
dp = Dispatcher()


@dp.message(CommandStart())
async def handle_start(message: types.Message):
    await message.answer(text=f"Hello, {message.from_user.full_name}!")


@dp.message()
async def aggregate_command(message: types.Message):
    payload: dict = json.loads(message.text)
    result = await aggregate_data(**payload)
    dataset_str = ", ".join(map(str, result["dataset"]))
    labels_str = ", ".join(map(str, result["labels"]))
    response = f'{{"dataset": [{dataset_str}], "labels": [{labels_str}]}}'

    await message.reply(response)


async def main():
    logging.basicConfig(level=logging.INFO)
    await dp.start_polling(bot)
