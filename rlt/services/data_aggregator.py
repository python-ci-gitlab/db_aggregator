import motor.motor_asyncio
from datetime import datetime
from typing import Dict, List

client = motor.motor_asyncio.AsyncIOMotorClient("mongodb://localhost:27017")
db = client.salary
collection = db.salary

group_format = {
    "hour": "%Y-%m-%dT%H:00:00",
    "day": "%Y-%m-%dT00:00:00",
    "month": "%Y-%m-01T00:00:00",
}


async def aggregate_data(dt_from: str, dt_upto: str, group_type: str) -> Dict[str, List]:
    dt_from_obj = datetime.fromisoformat(dt_from)
    dt_upto_obj = datetime.fromisoformat(dt_upto)
    dt_format = group_format.get(group_type)

    pipeline = [
        {"$match": {"dt": {"$gte": dt_from_obj, "$lte": dt_upto_obj}}},
        {"$group": {
            "_id": {"$dateToString": {"format": dt_format, "date": "$dt", "timezone": "UTC"}},
            "total": {"$sum": "$value"}
        }},
        {"$sort": {"_id": 1}}
    ]

    aggregated_results = collection.aggregate(pipeline)
    result = await aggregated_results.to_list(length=None)

    dataset = [doc["total"] for doc in result]
    labels = [doc["_id"] for doc in result]

    return {"dataset": dataset, "labels": labels}
